PACKAGE_LIST_FILE=$1

# Adding repository for python packages
sudo add-apt-repository ppa:deadsnakes/ppa

sudo apt-get update

# Installing ubuntu packages
< ${PACKAGE_LIST_FILE} xargs sudo apt-get install -y --fix-missing

# Optional: upgrade pip
# NOTE that this may solve problems in installing python dependencies with newer python versions
echo "upgrading pip"
curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10
