#!/bin/bash

VENV_NAME=$1

virtualenv -p /usr/bin/python3.10 ${VENV_NAME}
# Update pip in virtualenv. In ubuntu 20.04 it comes with a too old version when choosing pyton3.10
source ${VENV_NAME}/bin/activate
curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10
deactivate
